import reportWebVitals from './reportWebVitals';
import { Router } from '@reach/router';
import { Base } from './styled';
import './App.css';
import './i18n';

import Home from './pages/Home/Home';

import { DevOverlay } from '@ux';

const App = (): JSX.Element => (
  <Base>
    <Router>
      <Home path="/" />
    </Router>
    <DevOverlay />
  </Base>
);

reportWebVitals(metrics => {
  /*
  ga('send', 'event', {
    eventCategory: 'Web Vitals',
    eventAction: name,
    eventValue: Math.round(name === 'CLS' ? value * 1000 : value),
    eventLabel: id,
    nonInteraction: true,
  });
  */
});

export default App;
