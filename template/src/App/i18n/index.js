import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import en from './en';
import cz from './cz';

const i18nConfig = {
  resources: { en, cz },
  lng: 'en',
  fallbackLng: 'cz',
  interpolation: {
    escapeValue: false,
  },
};

i18n.use(initReactI18next).init(i18nConfig);
