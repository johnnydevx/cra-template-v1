import styled from 'styled-components';

export const Base = styled.div`
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p {
    margin: 0;
    padding: 0;
    line-height: 1.5;
  }

  h1 {
    font-size: 40px;
    color: #252525;
    font-family: 'Poppins';
  }

  h2 {
    font-size: 24px;
    color: #252525;
    font-family: 'Poppins';
  }

  h3 {
    font-size: 16px;
    color: #000000;
    font-weight: 500;
    font-family: 'Poppins';
  }

  p {
    font-size: 20px;
    color: #252525;
    font-weight: 400;
    font-family: 'Poppins';
  }

  .light {
    opacity: 0.75;
    font-size: 18px;
  }

  .invert {
    color: #fff;
  }

  .color {
    color: var(--theme);
  }
`;
