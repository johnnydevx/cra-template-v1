import { Base, Text } from './styled';
import { useTranslation } from 'react-i18next';

const Page = () => {
  const [t] = useTranslation<string>();

  return (
    <Base>
      <Text>{t('welcome')}</Text>
    </Base>
  );
};

export default Page;
