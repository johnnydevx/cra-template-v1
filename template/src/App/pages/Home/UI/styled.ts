import styled from 'styled-components/macro';

export const Base = styled.div`
  width: 100vw;
  height: 100vh;
  background: linear-gradient(45deg, #111, #000);
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Text = styled.div`
  font-size: 64px;
  font-weight: 200;
  color: #4448;
  user-select: none;
`;
