import UI from './UI/UI';
import { RouteComponentProps } from '@reach/router';

const Home = (props: RouteComponentProps) => {
  return <UI />;
};

export default Home;
