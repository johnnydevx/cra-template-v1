import React from 'react';
import { Base } from './styled';

const Component = ({ width = 3333, children }) => {
  return (
    <Base maxWidth={width}>
      { children }
    </Base>
  );
}

export default Component;