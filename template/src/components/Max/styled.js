import Styled from 'styled-components';

export const Base = Styled.div`;
  width: 100%;
  max-width: ${p => p.maxWidth}px;
`;