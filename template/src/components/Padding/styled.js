import Styled from 'styled-components';

// width: calc(100% - ${p => p.x * 2}px);
export const Base = Styled.div`;
  flex: 1;
  transition: 200ms;
  padding: ${p => p.y}px ${p => p.x}px;
`;