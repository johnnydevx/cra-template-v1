import React from 'react';
import { Base } from './styled';
import { useWindowSize } from 'utils';

const Component = ({ y = 0, x = 0, fixed, anim, children, style }) => {
  const size = useWindowSize();

  let m = 1;
  m = size.width < 1400 ? 0.5 : m;
  m = size.width < 560 ? 0.3 : m;
  m = fixed ? 1 : m;

  return (
    <Base y={y * m} x={x * m} className={anim} style={style}>
      { children }
    </Base>
  );
}

export default Component;