import React from 'react';
import TextField from '@material-ui/core/TextField';

const Component = ({ label, value, onChange,
  variant = 'filled',
  fullWidth = true,
  small = false,
  ...props
}) => {
  return (
    <TextField 
      variant={variant} 
      label={label}
      fullWidth={fullWidth}
      size={small ? 'small' : 'medium'}
      value={value}
      onChange={e => onChange(e.target.value)}
      {...props}
    />
  );
}

export default Component;