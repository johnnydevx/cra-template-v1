import React from 'react';
import Styled from 'styled-components';

const Avatar = Styled.div`
  width: ${p => p.size}px;
  height: ${p => p.size}px;
  border-radius: 8px;
  background: url('${p => p.src}'), #E7E7E7;
  background-position: center;
  background-size: cover;
  flex-shrink: 0;
`;

const AvatarComponent = ({ src, size = 48, ...props }) => ((
  <Avatar src={src} size={size} {...props} />
));

export default AvatarComponent;