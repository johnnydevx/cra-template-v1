import React from 'react';
import { Base, Text, H1, Link } from './styled';
import { Wrapper, Flex, Image, Space, Padding } from 'components';
import { useWindowSize } from 'utils';
import { navigate } from '@reach/router';

const Component = () => {
  const size = useWindowSize();

  return (
    <Base>
      <Wrapper>
        <Padding fixed x={32}>
          <Flex col={size.width < 800}>
            <Flex flex={1} col>
              <Image name="logo_dark" w={150} />
              <Space px={16} />
              <Text>Elit commodo est velit reprehenderit. Nulla exercitation enim tempor consequat cillum quis id ea in sunt aliqua sunt laboris nisi.</Text>
            </Flex>
            <Space px={32} />
            <Flex flex={1} col>
              <H1>Company</H1>
              <Link onClick={() => navigate('/')}>Home</Link>
              <Link>FAQ</Link>
              <Link>Testimonials</Link>
              <Link>Awesome Team</Link>
              <Link>Case Studies</Link>
              <Link>Blog</Link>
            </Flex>
            <Space px={32} />
            <Flex flex={1} col>
              <H1>Support</H1>
              <Link>Help Desk</Link>
              <Link>Documents</Link>
              <Link>Security</Link>
              <Link>Request</Link>
              <Link>Online Chat</Link>
              <Link>Manager</Link>
            </Flex>
            <Space px={32} />
            <Flex flex={1} col>
              <H1>Contact Info</H1>
              <Flex>
                <Text bold>Office</Text>
                <Text>13 Fulton street, Suite 721 City, State, Country</Text>
              </Flex>
              <Flex>
                <Text bold>Call us</Text>
                <Text>+880 123 456 789</Text>
              </Flex>
              <Flex>
                <Text bold>Email</Text>
                <Text>traduki@email.cz</Text>
              </Flex>
            </Flex>
          </Flex>
          <Space px={48} />
          <Flex x={2}>
            <Text>© 2021 Traduki.  All rights reserved.</Text>
          </Flex>          
        </Padding>
      </Wrapper>
    </Base>
  );
}

export default Component;