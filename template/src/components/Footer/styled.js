import Styled from 'styled-components';

export const Base = Styled.div`;
  padding: 32px 0;
  background: #F5F8FF;
  font-family: "Poppins";
`;

export const Text = Styled.div`
  font-size: 16px;
  line-height: 31px;
  color: #7a7d87;
  font-weight: 300;
  ${p => p.bold ? `
    color: #333333;
    font-weight: bold;
    margin-right: 16px;
  ` : ``}
`;

export const H1 = Styled.div`
  font-size: 20px;
  line-height: 30px;
  color: #333333;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const Link = Styled.div`
  font-size: 16px;
  line-height: 32px;
  color: #7a7d87;
  &:hover {
    color: #222;
    cursor: pointer;
  }
`;