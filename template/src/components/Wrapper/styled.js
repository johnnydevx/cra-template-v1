import Styled from 'styled-components';
import lessThan from 'utils/lessThan';

export const Base = Styled.div`;
  ${p => p.color ? `background-color: ${p.color};` : ``}
  ${p => p.image ? `
    background-image: url('${p.image}');
    background-size: ${p.size ? p.size : 'cover'};
    background-position: ${p.pos ? p.pos : 'center'};
    background-repeat: no-repeat;
  ` : ``}
  ${p => p.hideAt ? `
    ${lessThan(p.hideAt, `background-image: none;`)}
  ` : ``}
  ${p => p.shadows ? `
    box-shadow:
      ${p.shadows.map((s, i) => s)}
  ` : ``};
`;

export const Center = Styled.div`
  width: 100%;
  max-width: ${p => p.maxWidth}px;
  margin: 0 auto;
`;