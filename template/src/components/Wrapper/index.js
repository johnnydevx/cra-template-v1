import React from 'react';
import { Base, Center } from './styled';
import config from 'config';

const Component = ({ maxWidth = config.maxWidth, background = {}, children }) => {
  return (
    <Base 
      color={background.color} 
      image={background.image} 
      pos={background.position}
      shadows={background.shadows}
      size={background.size}
      hideAt={background.hideAt}
    >
      <Center maxWidth={maxWidth}>
        { children }
      </Center>
    </Base>
  );
}

export default Component;