import Styled from 'styled-components';

export const Base = Styled.div`;
  background: ${p => p.background ? p.background : `var(--theme)`};
  padding: ${p => p.padding};
  display: flex;
  justify-content: center;
  align-items: center;
  ${p => p.fullWidth ? `width: 100%;` : ``}
  border-radius: ${p => p.borderRadius};
  font-weight: ${p => p.fontWeight};
  font-size: ${p => p.fontSize};
  font-family: ${p => p.fontFamily};
  color: ${p => p.fontColor};
  box-sizing: border-box;
  transition: 200ms;
  user-select: none;
  &:hover {
    cursor: pointer;
    opacity: 0.75;
  } 
  &:active {
    transition: 50ms;
    opacity: 0.5;
  }
  ${p => p.disabled ? `
    filter: none;
    background: var(--button-disabled);
    &:hover {
      cursor: not-allowed;
      opacity: 1;
    } 
    &:active {
      opacity: 1;
    }
  ` : ``}
`;