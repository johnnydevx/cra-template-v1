import React from 'react';
import { Flex, Image, Space } from 'components';
import { Base } from './styled';
import config from 'config';

const Component = ({ text, disabled, mini, onClick, fullWidth = false,
  icon,
  background,
  fontColor = config.button.font.color,
}) => {
  return (
    <Base 
      onClick={disabled ? () => {} : onClick}
      disabled={disabled}
      mini={mini}
      background={background}
      fullWidth={fullWidth}
      padding={config.button.padding}
      borderRadius={config.button.borderRadius}
      fontFamily={config.button.font.family}
      fontWeight={config.button.font.weight}
      fontSize={config.button.font.size}
      fontColor={fontColor}
    >
      <Flex y={2}>
        { icon && <Image name={`icon/${icon}`} w="22px" /> }
        <Space px={10} />
        { text }
      </Flex>
    </Base>
  );
}

export default Component;