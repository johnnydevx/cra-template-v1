import React from 'react';
import { Base } from './styled';

const Component = ({ y, children }) => {
  return (
    <Base y={y}>
      { children }
    </Base>
  );
}

export default Component;