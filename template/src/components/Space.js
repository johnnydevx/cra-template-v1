const Space = ({ x, y, px }) => {
  return (
    <div style={{
      width: x || px,
      height: y || px,
    }} />
  );
};

export default Space;