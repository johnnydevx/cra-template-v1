import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const Component = ({ label, checked, onChange }) => {
  return (
    <FormControlLabel
      label={label}
      control={
        <Checkbox
          checked={checked}
          onChange={onChange}
          color="primary"
        />
      }
    />
  );
}

export default Component;