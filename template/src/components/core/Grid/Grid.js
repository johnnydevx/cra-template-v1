import React from 'react';
import Styled from 'styled-components';
import { lessThan } from 'utils';

const CSSGrid = Styled.div`
  display: grid;
  grid-template-rows: auto;
  grid-template-columns: repeat(${({ columns }) => columns}, 1fr);
  grid-gap: ${({ gap }) => gap > 0 ? `${gap}px` : 0};
  ${({ respo, columns }) => respo.map((n, i) => `
    ${lessThan(n, `grid-template-columns: repeat(${columns - i - 1}, 1fr);`)};
  `)}
`;

const Grid = ({ columns = 4, gap = 32, respo = [], children, ...rest }) => {
  return (
    <CSSGrid columns={columns} respo={respo} gap={gap} {...rest}>
      { children }
    </CSSGrid>
  );
}

export default Grid;