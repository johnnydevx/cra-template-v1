import React, { forwardRef } from 'react';
import Styled from 'styled-components';

const Pressable = Styled.div`
  user-select: none;
  transition: 200ms;
  background: ${p => p.colors[0]};
  &:hover {
    ${({ pointer }) => pointer && 'cursor: pointer;'}
    background: ${p => p.colors[1]};
  }  
  &:active {
    transition: 50ms;
    background: ${p => p.colors[2]};
  }
`;

const VARIANTS = [
  ['transparent', '#0001', '#0002'],
  ['#F7F7FF', '#e6e6f0', '#d5d5e3'],
];

const PressableComponent = (props, ref) => {
  const { colors = null, pointer = true, variant = 0, children, ...rest } = props;
  // add active { ...styles } object
  
  return (
    <Pressable 
      ref={ref}
      colors={colors ? colors : VARIANTS[variant]}
      pointer={pointer}
      {...rest}
    >
      { children }
    </Pressable>
  );
}

export default forwardRef(PressableComponent);