import React from 'react';
import { navigate } from '@reach/router';
import { Base, Wrapper, Item } from './styled';
import { Flex, Image, Button } from 'components';

const Component = ({ 
  background, height, maxWidth, items, theme, shadow, breakAt 
}) => {
  return (
    <Base 
      background={background} 
      theme={theme} 
      shadow={shadow} 
      height={height}
    >
      <Wrapper height={height} maxWidth={maxWidth} breakAt={breakAt}>
        <Flex y={2}>
          <Image name={`logo_${theme}`} w="auto" h="32px" onClick={() => navigate('/')} />
        </Flex>
        <Flex></Flex>
        <Flex y={2}>
          { items.map(item => {
            if (item.isButton) {
              return (<>
                <div style={{ width: 16 }} />
                <Button
                  key={item.label}
                  icon={item.icon}
                  text={item.label}
                  onClick={() => navigate(item.link)}
                />   
              </>);
            }
            return (
              <Item 
                key={item.label} 
                to={item.link}
                onClick={() => navigate(item.link)}
              >
                { item.label }
              </Item>              
            );
          })}
        </Flex>        
      </Wrapper>
    </Base>
  );
}

export default Component;