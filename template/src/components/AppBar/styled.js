import Styled from 'styled-components';
import lessThan from 'utils/lessThan';

export const Base = Styled.div`;
  background: ${p => p.background};
  height: ${p => p.height}px;
  ${p => p.shadow ? `
    filter: drop-shadow(0 0 43.5px rgba(116,125,135,0.18));
  ` : ``}
  ${lessThan(640, `
    height: auto;
  `)};
  color: ${p => p.theme === 'light' ? '#FFF' : '#333'};
`;

export const Wrapper = Styled.div`
  height: ${p => p.height}px;
  width: calc(100% - 32px);  
  max-width: ${p => p.maxWidth + 32}px;
  padding: 0 16px;
  margin: 0 auto;
  display: flex;
  align-items: center;
  justify-content: space-between;
  ${p => lessThan(p.breakAt, `
    flex-direction: column;
    height: auto;
    padding: 16px;
  `)};
`;

export const Item = Styled.div`
  font-size: 16px;
  font-weight: 500;
  margin-left: 24px;
  transition: 200ms;
  &:hover {
    text-decoration: underline;
    cursor: pointer;
  }
  ${lessThan(640, `
    margin-top: 16px;
  `)}
  ${p => p.isButton ? `
    border-radius: 32px;
    color: #FFF;
    padding: 12px 32px;
    background: var(--theme);
    &:hover {
      opacity: 0.76;
      text-decoration: none;
    }
    &:active {
      opacity: 0.5;
      transition: 50ms;
    }
    ${lessThan(800, `
      display: none;
    `)};
  ` : ``}
`;

export const _ = Styled.div``;