import React from 'react';
import AppBar from './AppBar.js';
import config from 'config';

const Component = ({ 
  maxWidth = config.maxWidth,
  background = config.appBar.background,
  height = config.appBar.height,
  theme = config.appBar.theme,
  shadow = config.appBar.shadow,
  items = config.appBar.items,
  loggedIn = false,
}) => {
  return (
    <AppBar
      background={background}
      height={height}
      maxWidth={maxWidth}
      theme={theme}
      shadow={shadow}
      items={items[loggedIn ? 'loggedIn' : 'loggedOut']}
      breakAt={config.appBar.breakAt}
    />
  );
}

export default Component;