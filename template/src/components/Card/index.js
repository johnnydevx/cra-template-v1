import React, { forwardRef } from 'react';
import { Base } from './styled';

const Component = forwardRef((props, ref) => {
  const { 
    padding = 16, fullWidth = false, minWidth = false, children, ...rest
  } = props;

  return (
    <Base 
      ref={ref} padding={padding} fullWidth={fullWidth} minWidth={minWidth} {...rest}
    >
      { children }
    </Base>
  );
});

export default Component;