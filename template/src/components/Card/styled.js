import Styled from 'styled-components';

export const Base = Styled.div`;
  background: #FFF;
  padding: ${p => p.padding}px;
  border-radius: 16px;
  filter: drop-shadow(0 0 20px rgba(152,156,166,0.24));
  ${p => p.fullWidth ? `
    width: calc(100% - ${p.padding * 2}px);
  ` : ``}
  ${p => p.minWidth ? `
    min-width: ${p.minWidth}px;
  ` : ``}
`;

export const _ = Styled.div``;