import React from 'react';
import Styled from 'styled-components';
import Asset from 'utils/Asset';

const Image = Styled.img`
  display: block;
  width: ${p => p.width ? p.width : '100%'};
  height: ${p => p.height ? p.height : 'auto'};
  ${p => p.shadow ? `filter: drop-shadow(0 0 16px rgba(127, 127, 127, 0.24));` : ``}
  transition: 100ms;
  user-select: none;
  ${p => p.clickable ? `
    &:hover {
      opacity: 0.75;
      cursor: pointer;
    }
    &:active {
      transition: 50ms;
      transform: scale(0.92);
    }
  ` : ``}
`;

const Component = ({ name, w, h, onClick = false, shadow = false, ...props }) => {
  return (
    <Image
      clickable={onClick}
      onClick={onClick ? onClick : undefined}
      src={Asset(name)} 
      width={w} 
      height={h} 
      alt="" 
      shadow={shadow} 
      draggable="false"
      {...props}
    />
  );
}

export default Component;