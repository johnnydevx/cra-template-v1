import React from 'react';
import Styled from 'styled-components';
import { useWindowSize } from 'utils';

const Flex = Styled.div`
  display: flex;
  ${p => p.flex ? `flex: ${p.flex};` : ``}
  flex-direction: ${p => p.dir};
  justify-content: ${p => p.x};
  align-items: ${p => p.y};
`;

const align = {
  row: {
    x: {
      1: 'flex-start',
      2: 'center',
      3: 'flex-end', 
      4: 'space-between',
      5: 'space-around',
    },
    y: {
      1: 'flex-start',
      2: 'center',
      3: 'flex-end',
    },
  },
  column: {
    y: {
      1: 'flex-start',
      2: 'center',
      3: 'flex-end',
      4: 'stretch',
    },
    x: {
      1: 'flex-start',
      2: 'center',
      3: 'flex-end',
    },
  },
};

const Component = ({ col, flex, x, y, breakAt, anim, style, children, ...props }) => {
  const size = useWindowSize();

  const dir = col ? 'column' : 'row';
  
  const breaking = breakAt && (size.width < breakAt);

  return (
    <Flex
      style={style}
      className={anim}
      flex={flex}
      dir={breaking ? 'column' : dir}
      x={align[dir]['x'][x]}
      y={align[dir]['y'][y]}
      {...props}
    >
      { children }
    </Flex>
  );
}

export default Component;