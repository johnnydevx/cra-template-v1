const config = {
  maxWidth: 1800,
  appBar: {
    height: 64,
    background: '#FFF',
    theme: 'light',
    breakAt: 420,
    transparent: true,
    shadow: false,
    items: {
      loggedOut: [
        { label: 'Sign Up/Login', link: '/login', isButton: true, icon: 'login' }
      ],
      loggedIn: [
        { label: 'Dashbhoard', link: '/main' },
        { label: 'Transactions', link: '/transactions' },
        { label: 'Pay/Send', link: '/send', isButton: true },
      ],
    },
  },
  button: {
    borderRadius: '4px',
    padding: '10px 24px',
    font: {
      family: 'Poppins',
      weight: '500',
      size: '16px',
      color: '#FFF',
    }
  }
};

export default config;