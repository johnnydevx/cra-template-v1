const lessThan = (breakpoint, styles) => {
  return `@media (max-width: ${breakpoint}px) {
    ${styles}
  }`;
};

export default lessThan;