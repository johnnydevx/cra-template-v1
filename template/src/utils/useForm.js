import { useState } from 'react';

const useForm = initialData => {
  const [data, setData] = useState(initialData);

  const set = (a, v) => {
    setData(prev => ({ ...prev, [a]: v }));
  };

  const input = name => {
    return {
      value: data[name],
      onChange: e => set(name, e.target.value),
    };
  };

  return [data, input, set];
};

export default useForm;
