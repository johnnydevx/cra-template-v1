const p = process.env.PUBLIC_URL;

const Asset = name => `${p}/asset/${name}.png`;

export default Asset;