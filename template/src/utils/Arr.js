// Immutable Array Utility
class Arr {
  static Set(og, index, value) {
    let arr = [...og];
    arr[index] = value;
    return [...arr];
  }

  static Remove(og, index) {
    return [...og.filter((_, i) => index !== i)];
  }
}

export default Arr;
