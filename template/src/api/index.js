const BASE_LINK = '';
const LOGIN_ROUTE = '/login';

class API {
  static get(link, callback = () => {}) {
    const id = localStorage.getItem('id');
    const token = localStorage.getItem('token');
    const args = { headers: { Authorization: `${id};${token}` } };

    fetch(BASE_LINK + link, args)
      .then(res => res.text())
      .then(data => {
        if (data === 'Not valid token') {
          localStorage.removeItem('id');
          localStorage.removeItem('token');
          window.location = LOGIN_ROUTE;
        }
        try {
          callback(JSON.parse(data));
        } catch (e) {
          callback(data);
        }
      });
  }

  static post(link, data, callback = () => {}) {
    const id = localStorage.getItem('id');
    const token = localStorage.getItem('token');
    const args = {
      method: 'POST',
      headers: {
        Authorization: `${id};${token}`,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: 'data=' + JSON.stringify(data),
    };

    fetch(BASE_LINK + link, args)
      .then(res => res.text())
      .then(backData => {
        if (data === 'Not valid token') {
          localStorage.removeItem('id');
          localStorage.removeItem('token');
          window.location = LOGIN_ROUTE;
        }
        try {
          callback(JSON.parse(backData));
        } catch (e) {
          callback(backData);
        }
      });
  }
}

export default API;
