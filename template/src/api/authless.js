const BASE_LINK = '';

class API {
  static get(link, callback = () => {}) {
    fetch(BASE_LINK + link)
      .then(res => res.text())
      .then(data => {
        try {
          callback(JSON.parse(data));
        } catch (e) {
          callback(data);
        }
      });
  }

  static post(link, data, callback = () => {}) {
    const args = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: 'data=' + JSON.stringify(data),
    };

    fetch(BASE_LINK + link, args)
      .then(res => res.text())
      .then(backData => {
        try {
          callback(JSON.parse(backData));
        } catch (e) {
          callback(backData);
        }
      });
  }
}

export default API;
