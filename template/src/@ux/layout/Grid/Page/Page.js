import React, { useState } from 'react';
import { Base, Sandbox } from './styled';
import Grid from '../Grid.js';
import Code from './Code/Code.js';
import { Clickable } from '@ux';

const Component = () => {
  const [props, setProps] = useState([
    { order: 1, name: 'columns', value: 2, defaultValue: 4, type: 'number' },
    { order: 1, name: 'gap', value: 32, defaultValue: 32, type: 'number' },
    { order: 1, name: 'respo', type: 'array', value: [], defaultValue: [] },
  ]);

  const getProp = prop => props.find(row => row.name === prop);

  const handlePropClick = prop => {
    if (prop.type === 'boolean') {
      setProps(prevProps => {
        return [
          ...prevProps.filter(p => p.name !== prop.name),
          { ...prop, value: !prop.value },
        ];
      }); 
    }
  };
 
  return (
    <Base>
      <Code 
        name="Grid" 
        props={props} 
        onPropClick={handlePropClick} 
      />
      <Sandbox>
        <Grid
          columns={getProp('columns').value}
          gap={getProp('gap').value}
          respo={getProp('respo').value}
        >
          <Clickable ripple="#FFF">A</Clickable>
          <Clickable ripple="#0FF">B</Clickable>
          <Clickable ripple="#F0F">C</Clickable>
          <Clickable ripple="#FF0">D</Clickable>
        </Grid>
      </Sandbox>
    </Base>
  );
}

export default Component;