import React from 'react';
import { Base, Line } from './styled';

const str = JSON.stringify;

const Code = ({ name, props, onPropClick }) => {
  return (
    <Base>
      <Line syntax>{ `<${name}` }</Line>
      { 
        props.sort((a, b) => a.order - b.order).map(prop => (
          <Line
            key={prop.order}
            tab={1} 
            isDefault={str(prop.value) === str(prop.defaultValue)}
            onClick={() => onPropClick(prop)}
            ripple="#FFF"
            type={prop.type}
          >
            { `${prop.name}={${str(prop.value)}}` }
          </Line>
        )) 
      }
      <Line syntax>{ `>` }</Line>
      
      <Line tab={1}>{  `<Clickable ripple="#FFF">A</Clickable>` }</Line>   
      <Line tab={1}>{  `<Clickable ripple="#0FF">B</Clickable>` }</Line>   
      <Line tab={1}>{  `<Clickable ripple="#F0F">C</Clickable>` }</Line>   
      <Line tab={1}>{  `<Clickable ripple="#FF0">D</Clickable>` }</Line>     

      <Line syntax>{ `</${name}>` }</Line>        
    </Base>
  );
};

export default Code;