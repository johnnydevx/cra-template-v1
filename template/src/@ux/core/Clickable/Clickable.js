import React, { forwardRef } from 'react';
import Styled from 'styled-components';
import { useRipple } from 'utils';

const CSSDiv = Styled.div`
  user-select: none;
  transition: 200ms;
  background: ${p => p.theme[0]};
  ${({ ripple }) => ripple && `
    position: relative;
    overflow: hidden;
  `}
  &:hover {
    ${({ pointer }) => pointer && 'cursor: pointer;'}
    background: ${p => p.theme[1]};
  }  
  &:active {
    transition: 50ms;
    background: ${p => p.theme[2]};
  }
`;

const Clickable = ({ 
  theme = ['#0000','#FFF1','#FFF2',{}],
  ripple = false,
  pointer = true,
  active = false,
  children, 
  ...rest 
}, ref) => {
  const [rippleHandler, rippleElements] = useRipple(ripple);

  return (
    <CSSDiv
      ref={ref}
      theme={theme}
      pointer={pointer}
      onMouseDown={rippleHandler}
      ripple={ripple}
      style={active ? theme[3] : {}}
      {...rest}
    >
      { ripple ? rippleElements : null }
      { children }
    </CSSDiv>
  );
}

// Todo:
// - add active styles props

export default forwardRef(Clickable);