import React, { useState } from 'react';
import { Base, Sandbox } from './styled';
import Clickable from '../Clickable.js';
import Code from './Code/Code.js';

const Component = () => {
  const [props, setProps] = useState([
    { order: 1, name: 'theme', type: 'array', value: ['#FFF2','#FFF4','#FFF6',{ padding: 16 }], defaultValue: [] },
    { order: 2, name: 'pointer', type: 'boolean', value: true, defaultValue: true },
    { order: 3, name: 'ripple', type: 'string', value: '#FFF', defaultValue: false },
    { order: 4, name: 'active', type: 'boolean', value: false, defaultValue: false },
  ]);

  const getProp = prop => props.find(row => row.name === prop);

  const handlePropClick = prop => {
    if (prop.type === 'boolean') {
      setProps(prevProps => {
        return [
          ...prevProps.filter(p => p.name !== prop.name),
          { ...prop, value: !prop.value },
        ];
      }); 
    }
  };
 
  return (
    <Base>
      <Code 
        name="Clickable" 
        props={props} 
        onPropClick={handlePropClick} 
      />
      <Sandbox>
        <Clickable
          theme={getProp('theme').value}
          pointer={getProp('pointer').value}
          ripple={getProp('ripple').value}
          active={getProp('active').value}
        >
          Hello
        </Clickable>
      </Sandbox>
    </Base>
  );
}

export default Component;