import React from 'react';
import { Base, Line } from './styled';

const str = JSON.stringify;

const Code = ({ props, onPropClick }) => {
  return (
    <Base>
      <Line syntax>{ `<Clickable` }</Line>
      { 
        props.sort((a, b) => a.order - b.order).map(prop => (
          <Line
            key={prop.order}
            tab={1} 
            isDefault={str(prop.value) === str(prop.defaultValue)}
            onClick={() => onPropClick(prop)}
            ripple="#FFF"
            type={prop.type}
          >
            { `${prop.name}={${str(prop.value)}}` }
          </Line>
        )) 
      }
      <Line syntax>{ `>` }</Line>
      <Line tab={1}>{  `Hello` }</Line>   
      <Line syntax>{ `</Clickable>` }</Line>        
    </Base>
  );
};

export default Code;