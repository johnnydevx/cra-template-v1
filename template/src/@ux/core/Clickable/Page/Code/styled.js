import Styled from 'styled-components';
import { Clickable } from '@ux';

export const Base = Styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  background: #0004;
  padding: 16px;
`;

export const Line = Styled(Clickable)`
  color: #FFF;
  font-size: 24px;
  font-family: Consolas;
  font-weight: 500;
  margin-left: ${({ tab = 0 }) => `${32 * tab}px;`}
  ${({ syntax }) => syntax && `color: #38d6b6; font-weight: 700;`}
  ${({ type }) => type === 'boolean' && `color: #5c5cff;`}
  ${({ type }) => type === 'number' && `color: #33d6a8;`}
  ${({ type }) => type === 'string' && `color: #e05f2b; font-weight: 700;`}
  ${({ isDefault }) => isDefault && `opacity: 0.5;`}
  cursor: default !important;
`;