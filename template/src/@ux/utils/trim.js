const trim = (text, maxLen) => {
  const trimmed = text.substr(0, maxLen);
  const ending = text.length > maxLen ?  `...` : '';
  return trimmed + ending;
};

export default trim;