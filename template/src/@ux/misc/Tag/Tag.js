import React from 'react';
import Styled from 'styled-components';

const Base = Styled.div`
  padding: 4px 12px;
  border-radius: 32px;
  font-size: 14px;
  font-weight: 500;
  background: ${p => p.color};
  color: ${p => p.textColor};
  text-align: center;
`;

const TagComponent = ({ color, text, textColor }) => {
  return (
    <Base color={color} textColor={textColor}>
      { text }
    </Base>
  );
}

export default TagComponent;