import React, { useState, useEffect } from 'react';
import UI from './UI/UI.js';

const DevelopmentOverlay = () => {
  const [isVisible, setIsVisible] = useState(false);

  const handleKeydown = e => {
    if (e.key !== 'F1') return false;
    e.preventDefault();
    setIsVisible(b => !b);
  };

  useEffect(() => {
    window.addEventListener('keydown', handleKeydown);
    return () => window.removeEventListener('keydown', handleKeydown);
  }, []);
  
  return (
    <UI 
      isVisible={isVisible} 
    />
  );
}

export default DevelopmentOverlay;