import React, { Suspense, lazy } from 'react';
import { Base } from './styled';

const Viewer = ({ route }) => {
  const path = route.substr(10);

  const ComponentPage = lazy(() => import(`@ux/${path}/Page/Page.js`));

  return (
    <Base>
      <Suspense fallback={<div />}>
        <ComponentPage />
      </Suspense>
    </Base>
  );
};

export default Viewer;