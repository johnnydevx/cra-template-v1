import Styled from 'styled-components';

export const Header = Styled.div`
  display: flex;
  padding: 8px 16px;
  color: #FFF;
  font-size: 24px;
  font-weight: 700;
  font-family: "Nunito Sans";
  align-items: flex-end;
`;

export const Base = Styled.div`
  height: 100vh;
  width: 240px;
  background: #0001;
  border-right: 1px solid #fff4;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const Label = Styled.div`
  font-size: 16px;
  font-weight: 500;
  color: #FFF;
  margin: 6px 0;
  margin-left: 16px;
`;