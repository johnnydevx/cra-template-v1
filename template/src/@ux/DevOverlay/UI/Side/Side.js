import { Base, Header, Label } from './styled';
import Item from './Item/Item.js';

const ROUTES = [
  { label: 'Core', path: 'components/core', count: 1 },
  { label: 'Layout', path: 'components/layout', count: 1 },
];

const Side = ({ route, onChange }) => {
  return (
    <Base>
      <div>
        <Header>V1.2</Header>
        <Label>COMPONENTS</Label>
        {ROUTES.map(({ label, path, count }, index) => (
          <Item
            key={label}
            active={path === route}
            isFirst={index === 0}
            label={label}
            count={count}
            onClick={() => onChange(path)}
          />
        ))}
      </div>
      <div>
        <Item
          active={route === 'i18n'}
          onClick={() => onChange('i18n')}
          label="i18n"
          count=""
          isFirst={false}
        />
      </div>
    </Base>
  );
};

export default Side;
