import React from 'react';
import { Base, SideItemText } from './styled';

const Item = ({ isFirst, active, label, count, onClick }) => {
  const activeStyle = {
    background: 'linear-gradient(to right, #FFF2, #fff0)'
  };

  return (
    <Base
      first={isFirst} 
      theme={['#0000','#FFF1','#FFF2', activeStyle]}
      active={active}
      pointer={false}
      onClick={onClick}
      ripple="#FFF"
    >
      <SideItemText>{ label }</SideItemText>
      <SideItemText>{ count }</SideItemText>
    </Base>
  );
};

export default Item;