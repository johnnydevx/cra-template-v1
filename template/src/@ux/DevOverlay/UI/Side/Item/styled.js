import Styled from 'styled-components';
import { Clickable } from '@ux';

export const Base = Styled(Clickable)`
  height: 40px;
  display: flex;
  box-sizing: border-box;
  padding: 0 16px;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid #fff4;
  ${p => p.first && `border-top: 1px solid #fff4;`}
`;

export const SideItemText = Styled.div`
  color: #FFF;
  font-size: 18px;
  font-weight: 500;
`;