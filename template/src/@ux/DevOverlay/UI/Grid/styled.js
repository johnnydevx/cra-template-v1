import Styled from 'styled-components';

export const Base = Styled.div`;
  padding: 16px;
`;

export const Header = Styled.div`
  color: #FFF;
  font-size: 32px;
  font-weight: 500;
  margin-bottom: 32px;
`;

export const Text = Styled.div`
  color: #FFFE;
  font-size: 16px;
  margin-bottom: 32px;
`;