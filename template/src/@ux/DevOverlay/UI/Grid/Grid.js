import React from 'react';
import { Base, Header } from './styled';
import Item from './Item/Item.js';
import { Grid } from '@ux';

const CONTENTS = {
  'components/core': {
    header: 'Core Components',
    items: [{ name: 'Clickable', path: 'core/Clickable' }],
  },
  'components/layout': {
    header: 'Layout Components',
    items: [{ name: 'Grid', path: 'layout/Grid' }],
  },
};

const GridComponent = ({ show, onClick }) => {
  const { header, items } = CONTENTS[show];

  return (
    <Base>
      <Header>{ header }</Header>
      <Grid columns={4} gap={16} respo={[1200,1000,800]}>
        { items.map(({ name, path }) => <Item key={path} label={name} onClick={() => onClick(`component/${path}`)} />) }
      </Grid>    
    </Base>
  );
};

export default GridComponent;