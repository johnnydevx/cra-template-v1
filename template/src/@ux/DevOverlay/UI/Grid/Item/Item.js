import React from 'react';
import { Base, Label } from './styled';

const Item = ({ label, onClick }) => {
  return (
    <Base 
      theme={['#fff1','#fff3','#fff5']}
      pointer={false}
      onClick={onClick}
      ripple="#FFF"
    >
      <Label>{ `<${label} />` }</Label>
    </Base>
  );
};

export default Item;