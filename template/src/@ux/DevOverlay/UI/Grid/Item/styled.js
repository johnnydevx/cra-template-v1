import Styled from 'styled-components';
import { Clickable } from '@ux';

export const Base = Styled(Clickable)`
  display: flex;
  align-items: center;
  justify-content: center;
  align-items: center;
  padding: 32px 16px;
`;

export const Label = Styled.div`
  color: #FFF;
  font-size: 24px;
  font-family: Consolas;
  font-weight: 700;
`;