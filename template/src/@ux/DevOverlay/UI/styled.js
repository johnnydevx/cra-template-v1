import Styled from 'styled-components';

export const Base = Styled.div`;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: #0009;
  z-index: 777;
  display: ${p => p.visible ? `flex` : `none`};
  transition: 200ms;
  user-select: none;
  box-sizing: border-box;
`;

export const Main = Styled.div`
  width: calc(100vw - 241px);
  height: 100vh;
`;