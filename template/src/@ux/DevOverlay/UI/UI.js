import React, { useState } from 'react';
import { Base, Main } from './styled';

import Side from './Side/Side.js';
import Grid from './Grid/Grid.js';
import Viewer from './Viewer/Viewer.js';

const OverlayUI = ({ isVisible }) => {
  const [route, setRoute] = useState('components/core');

  const isRoute = r => {
    const isRouteRight = route.substring(0, r.length) === r;
    const isSlashLast = route.charAt(r.length) === '/';
    return isRouteRight && isSlashLast;
  }

  return (
    <Base visible={isVisible}>
      <Side 
        route={route}
        onChange={r => setRoute(r)}
      />
      <Main>
        { isRoute('components') && (
          <Grid show={route} onClick={name => setRoute(name)} />
        ) }
        { isRoute('component') && (
          <Viewer route={route} />
        ) }
      </Main>
    </Base>
  );
}

export default OverlayUI;