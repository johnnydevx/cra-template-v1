# UX | v0.0.2

## To-do
- Space & Padding auto-scaling rework
- `box-sizing: border-box;` - solve `width: 100%` and `padding` problem once for all.
- use `normalize.css` (@import-normalize;)
- sidebar and appbar outside router
- useRespo hook
- pressable auto-navigate