# CRA Template V1

## Installation

### Yarn

```
yarn create react-app myapp --template v1
```

### NPM

```
npx create-react-app myapp --template v1
```

## Updates

### V1.2

- TypeScript
- Fast Refresh
- Transition to `styled-components/macro`
- New `api/` directory

### V1.1

- [Prettier](https://prettier.io/)
- [Web Vitals](https://create-react-app.dev/docs/measuring-performance/)
